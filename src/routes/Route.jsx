import React from 'react';
import { BrowserRouter, Routes, Route, useParams } from "react-router-dom";

import App from '../App'
import AboutUs from '../pages/AboutUs/AboutUs';
import AddWolf from '../pages/AddWolf/AddWolf';
import Home from '../pages/Home/Home';
import WolfList from '../pages/WolfList/WolfList';
import NotFound from '../pages/NotFound/NotFound'
import WolfDetails from '../pages/WolfDetails/WolfDetails';
import AdoptWolf from '../pages/AdoptWolf/AdoptWolf';

function RouteApp() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<App />}>
          <Route path='/' element={<Home />} />
          <Route path='/wolf-list' element={<WolfList />} />
          <Route path='/wolf-details/:id' element={<WolfDetails />} />
          <Route path='/adopt-wolf/:id' element={<AdoptWolf />} /> 
          <Route path='/add-wolf' element={<AddWolf />} />
          <Route path='/about-us' element={<AboutUs />} />
        </Route>

        <Route path='*' element={<NotFound />} />
      </Routes>
    </BrowserRouter>
  );
}

export default RouteApp;