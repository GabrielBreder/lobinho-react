import './App.css';

import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';

import { WolfProvider } from './context/WolfContext';

import { Outlet } from 'react-router-dom';

function App() {
  return (
    <div className="App">
      <WolfProvider>
        <Header />
        <Outlet />
        <Footer />
      </WolfProvider>
    </div>
  );
}

export default App;
