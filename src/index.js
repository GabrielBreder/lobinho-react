import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import RouteApp from './routes/Route';

ReactDOM.render(
  <RouteApp />,
  document.getElementById('root')
);

