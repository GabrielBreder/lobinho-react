import React, { useContext, useEffect, useState } from 'react';

import classes from './Home.module.css';

import Subtitle from '../../components/Subtitle/Subtitle';
import ValueCard from '../../components/ValueCard/ValueCard';
import WolfCard from '../../components/WolfCard/WolfCard';

import protectionImg from '../../assets/icons/protection-icon.svg';
import affectionImg from '../../assets/icons/care-icon.svg';
import companionshipImg from '../../assets/icons/companionship-icon.svg';
import rescueImg from '../../assets/icons/rescue-dog-icon.svg';

import { WolfContext } from '../../context/WolfContext';

function Home() {
  const context = useContext(WolfContext)

  useEffect(() => {
    context.getRandomWolves()
  }, [])

  const wolvesNotAdopted = context.randomWolves
  
  const cards = {
    protection: {
      img: protectionImg,
      title: "Proteção",
      description: "Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral."
    },
    affection: {
      img: affectionImg,
      title: "Carinho",
      description: "Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral."
    },
    companionship: {
      img: companionshipImg,
      title: "Companherismo",
      description: "Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral."
    },
    rescue: {
      img: rescueImg,
      title: "Resgate",
      description: "Assim mesmo, o desenvolvimento contínuo de distintas formas de atuação facilita a criação do sistema de participação geral."
    }
  }

  return (
    <>
      <section className={classes.main_container}>
        <div className={classes.center}>
          <h1>Adote um Lobinho</h1>
          <div className={classes.body_underline}></div>
          <div className={classes.txt}>
            <p>
              É claro que o consenso sobre a necessidade de qualificação apresenta
              tendências no sentido de aprovar a manutenção das regras de conduta
              normativas.
            </p>
          </div>
        </div>
      </section>

      <section className={classes.about_container}>
        <Subtitle text="Sobre"/>
        <p>
          Não obstante, o surgimento do comércio virtual faz parte de um processo
          de gerenciamento do levantamento das variáveis envolvidas. Não obstante,
          o surgimento do comércio virtual faz parte de um processo de
          gerenciamento do levantamento das variáveis envolvidas.Não obstante, o
          surgimento do comércio virtual faz parte de um processo de gerenciamento
          do levantamento das variáveis envolvidas.Não obstante, o surgimento do
          comércio virtual faz parte de um processo de gerenciamento do
          levantamento das variáveis envolvidas.
        </p>
      </section>


      <section className={classes.value_container}>
        <Subtitle text="Valores"/>
        <div className={classes.cards}>
          <ValueCard img={cards.protection.img} cardTitle={cards.protection.title} description={cards.protection.description} />
          <ValueCard img={cards.affection.img} cardTitle={cards.affection.title} description={cards.affection.description} />
          <ValueCard img={cards.companionship.img} cardTitle={cards.companionship.title} description={cards.companionship.description} />
          <ValueCard img={cards.rescue.img} cardTitle={cards.rescue.title} description={cards.rescue.description} />
        </div>
      </section>

      <section className={classes.example_container}>
        <Subtitle text="Lobos Exemplos"/>
    
        {
          wolvesNotAdopted.map(wolf => 
            <WolfCard 
              key={wolf.id}
              name={wolf.name}
              age={wolf.age}
              description={wolf.description}
              img={wolf.image_url}
            />
          )
        }
        
      </section>
    </>
  );
}

export default Home;