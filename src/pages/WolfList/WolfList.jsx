import React, { useContext, useEffect, useState } from 'react';

import classes from './WolfList.module.css';

import WolfCard from '../../components/WolfCard/WolfCard';
import SecondaryBtn from '../../components/SecondaryBtn/SecondaryBtn'

import searchIcon from '../../assets/icons/search_icon.svg';

import { Link, useNavigate } from 'react-router-dom';

import { WolfContext } from '../../context/WolfContext';

function WolfList() {

  const context = useContext(WolfContext)
  const navigate = useNavigate()

  useEffect(() => {
    context.getWolves()
  }, [])

  useEffect(() => {
    context.getAdoptedWolves()
  }, [])

  const wolvesNotAdopted = context.wolves
  const adoptedWolves = context.adoptedWolves

  function wolfDetails(wolfId) {
    navigate(`/wolf-details/${wolfId}`)
  }

  return (
    <>
      
      <section className={classes.search_container}>
        <div className={classes.search}>
          <div className={classes.input}>
            <input type="text" id="search-input" />
            <img
              src={searchIcon}
              alt="search"
              id="search-btn"
            />
          </div>
          <Link to={'/add-wolf'}>
            <SecondaryBtn text="+ Lobo" textColor="white" />
          </Link>
        </div>
      </section>

      {
        wolvesNotAdopted.map(wolf => 
          <WolfCard 
            key={wolf.id}
            name={wolf.name}
            age={wolf.age}
            description={wolf.description}
            img={wolf.image_url}

            onclick={() => {wolfDetails(wolf.id)}}
            showAdoptBtn={true}
            btnText="Adotar"
            textColor="white"
          />
        )
      }

      {
        adoptedWolves.map(wolf => 
          <WolfCard 
            key={wolf.id}
            name={wolf.name}
            age={wolf.age}
            description={wolf.description}
            img={wolf.image_url}
            
            showAdoptBtn={true}
            btnText="Adotado"
            btnColor="#7aac3a"
            textColor="white"
            canClick={false}
          />
          
        )
      }
    </>
  );



}

export default WolfList;