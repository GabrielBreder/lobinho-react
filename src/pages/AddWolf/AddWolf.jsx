import React, { useState } from 'react';

import classes from './AddWolf.module.css';

import { api } from '../../services/api';

import PrimaryBtn from '../../components/PrimaryBtn/PrimaryBtn';

function AddWolf() {

  const [name, setName] = useState('')
  const [age, setAge] = useState('')
  const [img, setImg] = useState('')
  const [description, setDescription] = useState('')
  
  
  function formInformation(e) {
    e.preventDefault()

    let body = {
      wolf: {
        name: name,
        age: age,
        image_url: img,
        description: description
      }
    }
    
    api.post('/wolves', body)
    .then(res => {
      alert(`Lobo ${res.data.name} adicionado com sucesso!`)
      window.location = '/'
    })
    .catch(err => console.log(err))
  }


  return (

    <section>
      <div className={classes.form_container}>
        <div className={classes.form_header}>
          <h1>Coloque um Lobinho para Adoção</h1>
        </div>
        <form action="" onSubmit={formInformation}>
          <div className={classes.form_body}>
            <div className={classes.name_age}>
              <div className={classes.name}>
                <p>Nome do Lobinho:</p>
                <input type="text" onChange={e => setName(e.target.value)} />
              </div>
              <div className={classes.age}>
                <p>Anos:</p>
                <input type="text" onChange={e => setAge(e.target.value)}/>
              </div>
            </div>
            <div className={classes.photo}>
              <p>Link da Foto:</p>
              <input type="text" onChange={e => setImg(e.target.value)}/>
            </div>
            <div className={classes.description}>
              <p>Descrição</p>
              <textarea name="" id="" cols="30" rows="10" onChange={e => setDescription(e.target.value)}></textarea>
            </div>
          </div>
          <div className={classes.form_footer}>
            <PrimaryBtn text="Salvar" type='submit' />
          </div>
        </form>
      </div>
    </section>

  );
}

export default AddWolf;