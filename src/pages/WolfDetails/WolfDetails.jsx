import React, { useContext, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

import classes from './WolfDetails.module.css';

import { WolfContext } from '../../context/WolfContext';
import SecondaryBtn from '../../components/SecondaryBtn/SecondaryBtn';

function WolfDetails() {

  const context = useContext(WolfContext)
  const navigate = useNavigate()

  const { id } = useParams()
  
  useEffect(() => {
    context.getWolfById(Number(id))
  }, [id])

  const wolf = context.wolfById

  function adoptWolf(wolfId) {
    navigate(`/adopt-wolf/${wolfId}`)
  }

  function deleteWolf(wolfId) {
    context.deleteWolf(wolfId)

    alert("O lobo foi deletado!")
    navigate('/')
    
  }

  return (
      <section>
        <div className={classes.wolf_header}>
          <h3 id="wolf-name">{wolf.name}</h3>
        </div>

        <div className={classes.wolf_container}>

            <div className={classes.img_btn_container}>
          
                <img src={wolf.image_url} alt="wolf" id="wolf-img" />
              

              <div className={classes.buttons_container}>
                <SecondaryBtn 
                  text="ADOTAR" 
                  btnColor="#7aac3a" 
                  textColor="white" 
                  onclick={() => {adoptWolf(id)}}
                />
                <SecondaryBtn 
                  text="EXCLUIR" 
                  btnColor="#de5959" 
                  textColor="white"
                  onclick={() => {deleteWolf(id)}}
                />
              </div>
            </div>
          
              <div className={classes.wolf_description}>
                
                <p id="wolf-description">
                  {wolf.description}
                </p>
            
              </div>
            </div>


          
      </section>
  );
}

export default WolfDetails;