import React, { useContext, useEffect, useState } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { WolfContext } from '../../context/WolfContext';
import PrimaryBtn from '../../components/PrimaryBtn/PrimaryBtn';

import classes from './AdoptWolf.module.css';

function AdoptWolf() {

  const [adopterName, setAdopterName] = useState('')
  const [adopterAge, setAdopterAge] = useState(0)
  const [adopterEmail, setAdopterEmail] = useState('')

  const navigate = useNavigate()
  const context = useContext(WolfContext)
  const wolf = context.wolfById
  const { id } = useParams()
  

  useEffect(() => {
    context.getWolfById(Number(id))
  }, [])


  const data = {
    "wolf": {
      adopter_name: adopterName,
      adopter_age: adopterAge,
      adopter_email: adopterEmail
    }
  }

  function adoptWolf() {
    context.adoptWolf(id, data)
    alert(`Lobo ${wolf.name} adotado com sucesso!`)
    navigate('/')
  }

  return (
    <section className={classes.section}>
      <div className={classes.section_header}>
        <img src={wolf.image_url} alt="" />
        <div className={classes.title_id}>
          <h1>{wolf.name}</h1>
          <p>ID: {wolf.id}</p>
        </div>
      </div>
      <div className={classes.form_body}>
        <div className={classes.name_age}>
          <div className={classes.name}>
            <p>Seu Nome:</p>
            <input type="text" id="adopter-name" onChange={e => setAdopterName(e.target.value)}/>
          </div>
          <div>
            <p>Idade:</p>
            <input type="text" id="adopter-age" onChange={e => setAdopterAge(e.target.value)}/>
          </div>
        </div>
        <div>
          <p>Email:</p>
          <input type="text" id="adopter-email" onChange={e => setAdopterEmail(e.target.value)}/>
        </div>
      </div>
      <div className={classes.form_footer}>
        <PrimaryBtn text="Adotar" onclick={adoptWolf} />
      </div>
    </section>
  );
}

export default AdoptWolf;