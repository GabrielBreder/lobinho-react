import React from 'react';

import classes from './AboutUs.module.css';

function AboutUs() {
  return (
    <section>
      <div class={classes.main_container}>
        <h1>Quem somos</h1>
        <p>
          Gingerbread cookie gingerbread jujubes cake biscuit tart fruitcake.
          Jujubes chupa chups wafer cheesecake lemon drops. Pastry caramels
          cookie marshmallow. Chupa chups jelly beans apple pie powder marzipan
          oat cake jelly-o chupa chups biscuit. Lollipop biscuit jujubes tootsie
          roll oat cake chocolate bar.
        </p>
      </div>
    </section>
  );
}

export default AboutUs;