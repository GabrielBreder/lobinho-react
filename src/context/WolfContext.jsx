import React, { createContext } from 'react';
import useWolves from './hooks/useWolves'

const WolfContext = createContext()

function WolfProvider( { children } ) {

  const { 
    getWolves, 
    wolves, 
    getRandomWolves, 
    randomWolves, 
    getAdoptedWolves, 
    adoptedWolves,
    getWolfById,
    wolfById,
    deleteWolf,
    adoptWolf
  
  } = useWolves()

  return (
    <WolfContext.Provider 
      value={ 
        { 
          getWolves, 
          wolves, 
          getRandomWolves, 
          randomWolves, 
          getAdoptedWolves, 
          adoptedWolves,
          getWolfById,
          wolfById,
          deleteWolf,
          adoptWolf
        } 
      }>
      { children }
    </WolfContext.Provider>
  );
}

export { WolfContext, WolfProvider }
