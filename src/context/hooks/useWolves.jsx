import React, { useEffect, useState } from 'react';
import { api } from '../../services/api';

export default function useWolves(wolfId) {
 
  const [wolves, setWolves] = useState([])
  const [randomWolves, setRandomWolves] = useState([])
  const [adoptedWolves, setAdoptedWolves] = useState([])
  const [wolfById, setWolfById] = useState([])
  
  const getWolves = () => {
    api.get('/wolves')
    .then(res => setWolves(res.data))
    .catch(err => console.log(err))
  }

  const getRandomWolves = () => {
    api.get('/wolves')
    .then(res => {
      const data = res.data
      const length = data.length
      
      const randonWolf = () => {
        return Math.floor(Math.random() * (length - 1))
      }

      const numFirstRandonWolf = randonWolf()
      const numSecondRandonWolf = randonWolf()

      const firstRandonWolf = res.data[numFirstRandonWolf]
      const secondRandonWolf = res.data[numSecondRandonWolf]

      setRandomWolves([firstRandonWolf, secondRandonWolf])

    })
    .catch(err => console.log(err))
  }

  const getAdoptedWolves = () => {
    api.get('/wolves/adopted')
    .then(res => setAdoptedWolves(res.data))
    .catch(err => console.log(err))
  }

  const getWolfById = wolfId => {
    api.get(`/wolves/${wolfId}`)
    .then(res => setWolfById(res.data))
    .catch(err => console.log(err))
  }

  const deleteWolf = wolfId => {
    api.delete(`/wolves/${wolfId}`)
  } 

  const adoptWolf = (wolfId, data) => {
    
    api.put(`/wolves/${wolfId}`, data)
    .then(res => res)
    .catch(err => console.log(err))
  }


  return {
    getWolves, 
    wolves, 
    getRandomWolves, 
    randomWolves, 
    getAdoptedWolves, 
    adoptedWolves,
    getWolfById,
    wolfById,
    deleteWolf,
    adoptWolf
  }
}
