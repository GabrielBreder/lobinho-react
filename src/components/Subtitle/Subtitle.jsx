import React from 'react';

import classes from './Subtitle.module.css';

function Subtitle({text}) {
  return (
    <h2 className={classes.subtitle}>{text}</h2>
  );
}

export default Subtitle;