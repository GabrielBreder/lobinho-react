import React from 'react';

import classes from './SecondaryBtn.module.css';

function SecondaryBtn({text, btnColor, textColor, canClick, onclick}) {

  let cursor = "pointer"
  let disabled = false
 
  if (canClick === false) {
    cursor = "default"
    disabled = true
  }
  
  const style = {
    backgroundColor: btnColor,
    color: textColor,
    cursor: cursor
  }

  return (
    <button 
      className={classes.secondary_btn} 
        id="footer-btn" 
        style={style}
        disabled={disabled}
        onClick={onclick}
        >
          {text}
    </button>
  );
}

export default SecondaryBtn;