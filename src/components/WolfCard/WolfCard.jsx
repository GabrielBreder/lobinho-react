import React from 'react';

import classes from './WolfCard.module.css';

import SecondaryBtn from '../SecondaryBtn/SecondaryBtn'

function WolfCard(props) {

  return (
    <div className={classes.wolf_container}>
      
      <div className={classes.wolf_img}>
        <img src={props.img} alt="wolf" />
      </div>
    
      <div className={classes.wolf_description}>
        <div className={classes.wolf_header}>
          <div className={classes.name_age_container}>
            <h3>{props.name}</h3>
            <p>Idade: {props.age} anos</p>
          </div>
          {props.showAdoptBtn && 
            <SecondaryBtn 
              text={props.btnText} 
              btnColor={props.btnColor} 
              textColor={props.textColor} 
              canClick={props.canClick} 
              wolfId={props.wolfId}
              onclick={props.onclick}
            /> 
          }
        </div>
        <div className={classes.wolf_body}>
          <p>
            {props.description}
          </p>
        </div>
      </div>
    </div>
  );
}

export default WolfCard;