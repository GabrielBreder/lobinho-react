import React from 'react';

import classes from './ValueCard.module.css';

function ValueCard(props) {
  return (
    <div className={classes.card}>
          <div className={classes.img}>
            <img src={props.img} />
          </div>
          <h3>{props.cardTitle}</h3>
          <p>
            {props.description}
          </p>
        </div>
  );
}

export default ValueCard;