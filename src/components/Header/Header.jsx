import React from 'react';

import classes from './Header.module.css';

import wolfIcon from '../../assets/icons/wolf-icon.svg'

import { Link } from 'react-router-dom';

function Header() {
  return (
    <header>
      <div className={classes.header_container}>
        <div className={classes.wolf_list}>
          <Link to={'/wolf-list'}>
            <h4>Nossos Lobinhos</h4>
          </Link>
        </div>
        <Link to={'/'}>
          <img src={wolfIcon} />
        </Link>
        <div className={classes.about_us}>
          <Link to={'/about-us'}>
            <h4>Quem Somos</h4>
          </Link>
        </div>
      </div>
    </header>
  );
}

export default Header;