import React from 'react';

import classes from './PrimaryBtn.module.css';

function PrimaryBtn({text, type, onclick}) {
  return (
    <button 
      className={classes.primary_btn} 
      id="save-btn" 
      type={type}
      onClick={onclick}
    >
      <p>{text}</p>
    </button>
  );
}

export default PrimaryBtn;