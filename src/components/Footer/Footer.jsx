import React from 'react';

import classes from './Footer.module.css';

import localIcon from '../../assets/icons/local-icon.svg'
import phoneIcon from '../../assets/icons/phone-icon.svg'
import emailIcon from '../../assets/icons/email-icon.svg'
import pawsIcon from '../../assets/images/paws.png'

import SecondaryBtn from '../SecondaryBtn/SecondaryBtn';

function Footer() {
  return (
    <footer>
      <div className={classes.footer}>
        <div className="map">
          <iframe
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14701.399403880656!2d-43.1389299194178!3d-22.900463392564642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983d71a922e81%3A0x4fa7e811241855c6!2sS%C3%A3o%20Domingos%2C%20Niter%C3%B3i%20-%20RJ!5e0!3m2!1spt-BR!2sbr!4v1645307932405!5m2!1spt-BR!2sbr"
            width="300"
            height="300"
            style={{ border: 0 }}
            allowFullScreen=""
            loading="lazy"
          ></iframe>
        </div>
        <div className={classes.details}>
          <div className={classes.address}>
            <img src={localIcon} alt="location icon" />
            <p>
              Av. Milton Tavares de Souza, s/n - Sala 115 B - Boa Viagem,
              Niterói - RJ,24210-315
            </p>
          </div>
          <div className={classes.phone}>
            <img src={phoneIcon} alt="phone icon" />
            <p>(99) 99999-9999</p>
          </div>
          <div className={classes.email}>
            <img src={emailIcon} alt="email icon" />
            <p>salve-lobos@lobINhos.com</p>
          </div>
          <SecondaryBtn text="Quem Somos" />
        </div>
        <div className={classes.paws}>
          <p>Desenvolvido com</p>
          <img src={pawsIcon} alt="paws icon" />
        </div>
      </div>
    </footer>
  );
}

export default Footer;